#!/usr/bin/env bash

echo "You must make sure that your kernel is build with SELinux support!"
read -r -p "$(tput bold)Choose options (Y/n): $(tput sgr0)" SELINUX_INSTALL

if [[ $SELINUX_INSTALL=~ ^([yY][eE][sS]|[yY])$ ]]; then
    echo 'Adding SELinux Repository...'
    selinux_repo_check

    sudo pacman -Sy --noconfirm base-selinux base-devel-selinux checkpolicy mcstrans libselinux libsemanage libsepol policycoreutils restorecond secilc selinux-dbus-config selinux-gui selinux-python selinux-python2 selinux-sandbox semodule-utils selinux-refpolicy-arch

    echo 'Adding Kernel Paramaters...'
    selinux_param=("lsm=landlock,lockdown,yama,integrity,selinux,bpf")

    for param in "${selinux_param[@]}"; do
        if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$param") ]]; then
            echo "The $param parameter has been added!"
            elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $param) != $param ]]; then
            sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$param"'|' /etc/default/grub
            echo "Paramater $param succefully added!"
        fi
    done
fi