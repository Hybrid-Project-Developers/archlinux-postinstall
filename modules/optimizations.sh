#!/usr/bin/env bash

chaotic_aur_check

clear

services_install(){
    echo "Select which services to install (several services can be installed at once)
    1 - Ananicy - Assign a higher priority to programs
    2 - Nohang - Memory monitoring and overflow
    3 - IrqBalance - Load balancing among all the cores
    4 - DBus-Broker - Improved version of the classic DBus
    5 - RNG-Tools - Entropy monitoring
    6 - Preload - Linking libraries with frequently used applications"
    
    read -r -p "$(tput bold)Choose daemons (1-5): $(tput sgr0)" SERVICES_PACKAGES
    if [[ $SERVICES_PACKAGES =~ [1-6] ]]; then
        if [[ $SERVICES_PACKAGES =~ [1] ]]; then
            package1=ananicy-cpp
            package2=ananicy-rules-git
        fi
        
        if [[ $SERVICES_PACKAGES =~ [2] ]]; then
            package3=nohang
        fi
        
        if [[ $SERVICES_PACKAGES =~ [3] ]]; then
            package4=irqbalance
        fi
        
        if [[ $SERVICES_PACKAGES =~ [4] ]]; then
            package5=dbus-broker
        fi
        
        if [[ $SERVICES_PACKAGES =~ [5] ]]; then
            package6=rng-tools
        fi

        if [[ $SERVICES_PACKAGES =~ [6] ]]; then
            package7=preload
        fi
        PACKAGE_INSTALL="$package1 $package2 $package3 $package4 $package5 $package6 $package7"
        
        if [[ $PACKAGE_INSTALL =~ [A-Za-z] ]]; then
            sudo pacman -Sy --needed $PACKAGE_INSTALL
        else
            echo "No packages are listed!"
        fi
        
        if [[ $PACKAGE_INSTALL =~ "ananicy-cpp" ]]; then
            service1=ananicy-cpp
        fi
        
        if [[ $PACKAGE_INSTALL =~ "nohang" ]]; then
            service2=nohang-desktop
        fi
        
        if [[ $PACKAGE_INSTALL =~ "irgbalance" ]]; then
            service3=irqbalance
        fi
        
        if [[ $PACKAGE_INSTALL =~ "dbus-broker" ]]; then
            service4=dbus-broker
        fi
        
        if [[ $PACKAGE_INSTALL =~ "rng-tools" ]]; then
            service5=rngd
        fi

        if [[ $PACKAGE_INSTALL =~ "preload" ]]; then
            service6=preload
        fi
        
        SERVICE_ENABLE="$service1 $service2 $service3 $service4 $service5 $service6"
        
        if [[ $PACKAGE_INSTALL =~ [A-Za-z] ]]; then
            systemctl enable --now $SERVICE_ENABLE
        fi
    else
        typing_protection
        services_install
    fi
    
    echo "Basic services activated!"
    
    echo "Checking the drive for TRIM support..."
    
    system_drive=$(mount | grep -w '/' | cut -d ' ' -f1 | tail -c 5 | cut -c -3)
    
    if [[ $(cat /sys/block/$system_drive/queue/rotational) = 1 ]]; then
        echo "The root partition is on the HDD, TRIM is not needed!"
        
        elif [[ $(cat /sys/block/$system_drive/queue/rotational) = 0 ]]; then
        echo "The root partition is on an SSD. I run the TRIM service..."
        
        systemctl enable --now fstrim.timer
        
        sudo fstrim -v /
    fi
    
    home_drive=$(mount | grep -w '/home' | cut -d ' ' -f1 | tail -c 5 | cut -c -3)
    
    if [[ $(cat /sys/block/$home_drive/queue/rotational) = 1 ]]; then
        echo "The home partition is on the HDD, TRIM is not needed!"
        
        elif [[ $(cat /sys/block/$home_drive/queue/rotational) = 0 ]]; then
        echo "The home partition is on an SSD. Running the TRIM service..."
        
        systemctl enable --now fstrim.timer
        
        sudo fstrim -v /home
    fi
}

io_scheduler(){
    echo "Setting up an udev rule for the I/O scheduler..."
    
    # Проверка наличия файла 60-ioschedulers.rules директории /etc/udev/rules.d/
    if [[ -f /etc/udev/rules.d/60-ioschedulers.rules ]]; then
        echo "The file 60-ioschedulers.rules already exists!"
    else
        echo "File 60-ioschedulers.rules does not exist, creating..."
        sudo touch /etc/udev/rules.d/60-ioschedulers.rules
    fi
    io_rules=("# set scheduler for NVMe devices" 'ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="none"' "# set scheduler for SSD and eMMC devices" 'ACTION=="add|change", KERNEL=="sd[a-z]*|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"' "# set scheduler for rotating disks" 'ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"')
    
    # Если есть файл 60-ioschedulers.rules, то добавить строчки из массива io_rules в него с помощью sed
    if [[ -f /etc/udev/rules.d/60-ioschedulers.rules ]]; then
        for i in "${io_rules[@]}"; do
            sudo sed -i "/# set scheduler for NVMe devices/i $i" /etc/udev/rules.d/60-ioschedulers.rules
        done
    fi
    echo "I/O scheduler setup successful!"
}

mesa_optimization(){
    if [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(AMD).*/\1/p') = AMD ]] || [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(Intel).*/\1/p') = Intel ]]; then
        
        echo "Installing the mesa-tkg-git package..."
        read -r -p "Continue? [y/N] " mesa_git_install

        if [[ $mesa_git_install =~ ^([yY][eE][sS]|[yY])$ ]]; then
            sudo pacman -S --needed mesa-tkg-git lib32-mesa-tkg-git
        fi

        if $amd_vulkan_driver = 1; then
            echo "Installing the amdvlk-git package"
            read -r -p "Continue? [y/N] " amdvlk_git_install
            if [[ $amdvlk_git_install =~ ^([yY][eE][sS]|[yY])$ ]]; then
                if $aur_install = ""; then
                    aur_helper_check
                    optimizations_module
                else
                    $aur_install amdvlk-git lib32-amdvlk-git
                fi
            fi
        elif $amd_vlk_driver = 2; then
            echo "Installing the vulkan-radeon-git package"
            read -r -p "Continue? [y/N] " vulkan_radeon_git_install
            
            if [[ $vulkan_radeon_git_install =~ ^([yY][eE][sS]|[yY])$ ]]; then
                aur_helper_check
                $aur_install vulkan-radeon-git lib32-vulkan-radeon-git
            fi
        fi
    fi            

    if [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(AMD).*/\1/p') = AMD ]]; then
        echo "Turning on the Smart Access Memory option..."
        echo "ВNote, you must make sure that "Re-Size BAR Support" and "Above 4G Decoding" are enabled in the BIOS/UEFI, otherwise refuse!"
        read -r -p "Continue? [y/N] " amd_sam_enable

        if [[ $amd_sam_enable =~ ^([yY][eE][sS]|[yY])$ ]]; then
            if [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) = radeon ]]; then
                sudo sh -c "echo 'radeonsi_enable_sam=true' >> /etc/environment"
            else
                sudo sh -c "echo 'RADV_PERFTEST=sam' >> /etc/environment"
            fi
        fi
    fi
}

grub_parameters(){
    lpj_count=$(sudo dmesg | grep lpj | sed -n 's/.*(\(.*\)[0-9]*)$/\1/p')
    grub_additional_params_array=("noibrs" "noibpb" "nopti" "nospectre_v2" "nospectre_v1" "l1tf=off" "nospec_store_bypass_disable" "no_stf_barrier" "mds=off" "tsx=on" "tsx_async_abort=off" "mitigations=off" "audit=0" "$lpj_count")
    
    for param in "${grub_additional_params_array[@]}"; do
        if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$param") ]]; then
            echo "The $param parameter has been added!"
            elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $param) != $param ]]; then
            sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$param"'|' /etc/default/grub
            echo "Paramater $param succefully added!"
        fi
    done
    
    sudo grub-mkconfig -o /boot/grub/grub.cfg
}

makepkg_optimization(){
    echo "Optimizing makepkg..."
    if [[ $(grep 'CFLAGS="-march=native -mtune=native -O2' /etc/makepkg.conf) ]]; then
        echo "Flags are already prescribed!"
    else
        sudo sed -i 's/-march=x86-64 -mtune=generic/-march=native -mtune=native/g' /etc/makepkg.conf
        
        sudo sed -i '/LDFLAGS/d' /etc/makepkg.conf
        
        sudo sed -i '/LTOFLAGS/d' /etc/makepkg.conf
        
        sudo sed -i '/CXXFLAGS="$CFLAGS.*"/a RUSTFLAGS="-C opt-level=3"' /etc/makepkg.conf
        
        sudo sed -i '/#MAKEFLAGS="-j2"/d' /etc/makepkg.conf
        
        sudo sed -i '/RUSTFLAGS="-C opt-level=3"/a MAKEFLAGS="-j$(nproc) -l$(nproc)"' /etc/makepkg.conf
        
        sudo sed -i '/MAKEFLAGS="-j$(nproc) -l$(nproc)"/a OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug !lto)' /etc/makepkg.conf
        
        grep -v "#" /etc/makepkg.conf | grep "BUILDENV=(.*)" | sudo sed -i 's/!ccache/ccache/g' /etc/makepkg.conf
    fi
    
    echo "Optimization of makepkg is successful!"
}

clang_llvm_install(){
    echo "Warning: Using Clang to build some packages may cause build errors. Do you want to continue? (y/n)"
    read -r clang_llvm_install_answer
    
    if [[ $clang_llvm_install_answer =~ ^([yY][eE][sS]|[yY])$ ]]; then
        echo "Installing Clang and LLVM..."
        sudo pacman -S --needed clang llvm lld
        
        echo "Installation of Clang and LLVM completed successfully!"
    else
        echo "Clang and LLVM installation interrupted!"
    fi
    
    echo "Adding Clang to makepkg..."
    
    clang_variables_array=("export CC=clang" "export CXX=clang++" "export LD=ld.lld" "export CC_LD=lld" "export CXX_LD=lld" "export AR=llvm-ar" "export NM=llvm-nm" "export STRIP=llvm-strip" "export OBJCOPY=llvm-objcopy" "export OBJDUMP=llvm-objdump" "export READELF=llvm-readelf" "export RANLIB=llvm-ranlib" "export HOSTCC=clang" "export HOSTCXX=clang++" "export HOSTAR=llvm-ar" "export HOSTLD=ld.lld")
    
    # Условие для проверки наличия параметра из массива clang_variables_array в файле /etc/makepkg.conf
    if [[ $(grep -E "^$clang_variables_array" /etc/makepkg.conf) ]]; then
        echo "The parameters are already added!"
    else
        # Цикл для добавление параметров в файл /etc/makepkg.conf с помощью sed с помощью массива clang_variables_array после строчки 'CHOST="x86_64-pc-linux-gnu"'
        for param in "${clang_variables_array[@]}"; do
            if [[ $(grep 'CHOST="x86_64-pc-linux-gnu"' /etc/makepkg.conf | grep "$param") ]]; then
                echo "The $param parameter has been added!"
                elif [[ $(grep 'CHOST="x86_64-pc-linux-gnu"' /etc/makepkg.conf | grep $param) != $param ]]; then
                sudo sed -i 's|CHOST="x86_64-pc-linux-gnu"|& '"$param"'|' /etc/makepkg.conf
                echo "Paramater $param succefully added!"
            fi
        done
    fi
}
zram_install(){
    echo "Installing ZRAM..."
    
    sudo pacman -S --needed zram-generator
    
    # Создание файла zram-generator.conf в пути /etc/systemd/
    sudo touch /etc/systemd/zram-generator.conf

    if [ -f "/etc/systemd/zram-generator.conf" ]; then
        echo "File \"/path/to/file\" exists"
    else
        sudo touch /etc/systemd/zram-generator.conf
    fi
    
    # Выбор компрессии для сжатия данных в файле zram-generator.conf
    echo "Select the compression for data compression in the zram-generator.conf file:
1. Low (lzo)
2. Medium (lz4)
3. High (zstd)
    Enter the compression number: "
    read -r compression_type_answer
    
    if [[ $compression_type_answer =~ ^([1-3])$ ]]; then
        if [[ $compression_type_answer =~ ^([1])$ ]]; then
            compression_type="lzo"
            elif [[ $compression_type_answer =~ ^([2])$ ]]; then
            compression_type="lz4"
            elif [[ $compression_type_answer =~ ^([3])$ ]]; then
            compression_type="zstd"
        fi
    else
        echo "Wrong input!"
        typing_protection
    fi
    
    # Цикл для записи в файл zram-generator.conf количества блоков памяти для сжатия данных и компрессии данных с учётом cores_count с помощью sudo sh echo
    # Пример текста
    # [zram$cores_count]
    # zram-size= ram / 2
    # compression-algorithm = lz4
    for ((i=0; i<$(nproc); i++)); do
        sudo sh -c 'echo [zram'"$i"']' >> /etc/systemd/zram-generator.conf
        sudo sh -c 'echo "zram-size=ram / 2"' >> /etc/systemd/zram-generator.conf
        sudo sh -c 'echo -e "compression-algorithm='$compression_type'"\\n' >> /etc/systemd/zram-generator.conf
    done
    
    for ((i=0; i<$cores_count; i++)); do
        systemctl start /dev/zram$i
        echo "The zram$i service is up and running!"
    done
    
}

PS3="Select the menu option: "

select start in Execute\ the\ entire\ script Choose\ what\ you\ need; do
    case $start in
        Execute\ the\ entire\ script)
            services_install
            io_scheduler
            grub_parameters
            makepkg_optimization
            clang_llvm_install
            zram_install
        ;;
        
        Choose\ what\ you\ need)
            select optimizations in Installing\ the\ services Setting\ up\ the\ I/O\ scheduler\ for\ the\ drives Mesa\ Optimization Settings\ GRUB\ Options Package\ compilation\ optimization Installing\ Clang\ and\ LLVM Launch\ ZRam Exit; do
                case $optimizations in
                    Installing\ the\ services)
                        services_install
                    ;;
                    
                    Setting\ up\ the\ I/O\ scheduler\ for\ the\ drives)
                        io_scheduler
                    ;;
                    Mesa\ Optimization)
                        mesa_optimization
                    ;;
                    Settings\ GRUB\ Options)
                        grub_parameters
                    ;;
                    
                    Package\ compilation\ optimization)
                        makepkg_optimization
                    ;;
                    
                    Installing\ Clang\ and\ LLVM)
                        clang_llvm_install
                    ;;
                    
                    Launch\ ZRam)
                        zram_install
                    ;;
                    
                    Exit)
                        exit
                    ;;
                esac
            done
        ;;
    esac
done