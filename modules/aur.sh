#!/usr/bin/env bash

chaotic_aur_check

clear

cas(){
    clear
    REPLY=
}

aur_helpers(){
    echo "AUR-helpers install..."
    sleep 5
    
    echo "Choose AUR-helper:
    1 - Yay
    2 - Pikaur
    3 - Pacaur
    4 - Pakku-git
    5 - Trizen
    6 - Paru"
    
    echo -n "Choose number: "
    read AUR_HELPER
    
    if [[ "$AUR_HELPER" = 1 ]]; then
        echo "Installing Yay - Yet another yogurt..."
                
        sudo pacman -Sy yay
        
        echo "Yay succefully installed!"
        aur_install=yay
        
    elif [[ "$AUR_HELPER" = 2 ]]; then
        echo "Installing Pikaur..."
                
        sudo pacman -Sy pikaur-git
        
        echo "Pikaur succefully installed!"
        aur_install=pikaur
        
    elif [[ "$AUR_HELPER" = 3 ]]; then
        "Instaling Pacaur... Dependences install..."
                
        sudo pacman -Sy expac yajl
        
        echo "The base dependencies are installed! Installing additional dependencies..."
                
        mkdir ~/.pacaur_tmp
        cd ~/.pacaur_tmp
        
        echo "Installing additional dependencies... Installing Cower..."
                
        gpg --recv-keys --keyserver hkp://pgp.mit.edu 1EB2638FF56C0C53
        
        curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cower
        
        makepkg -i PKGBUILD --noconfirm
        
        echo "Cower was installed! Installing Pacaur..."
                
        curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=pacaur
        
        makepkg -i PKGBUILD --noconfirm
        
        echo "Pacaur installed! Cleaning..."
        cd ~
        rm -rf ~/.pacaur_tmp

        aur_install=pacaur
        
    elif [[ "$AUR_HELPER" = 4 ]]; then
        echo "Installing Pakku..."
                
        echo "Creating temporary directory..."

        mkdir ~/.pakku_tmp
        cd ~/.pakku_tmp
        
        echo "Directory created! Cloning sources..."

        git clone https://aur.archlinux.org/pakku-git.git
        cd pakku-git
        
        makepkg -si --noconfirm
        
        echo "Pakku succefully installed! Cleaning..."
        cd ~
        rm -rf ~/.pakku_tmp

        aur_install=pakku
        
    elif [[ "$AUR_HELPER" = 5 ]]; then
        echo "Installing Trizen..."
                
        sudo pacman -Sy trizen-git
        
        echo "Trizen succefully installed!"
        
        aur_install=trizen

    elif [[ "$AUR_HELPER" = 6 ]]; then
        echo "Installing Paru..."
                
        sudo pacman -Syu paru-git
        
        echo "Paru succefully installed!"

        aur_install=paru
    fi
}

pamac_install(){
    echo "Installing Pamac..."
    
    sudo pacman -Sy pamac-aur archlinux-appstream-data-pamac
    
    echo "Pamac installed! Checking for installing add-ons..."
        
    if [[ $DESKTOP_SESSION = "plasma" ]]; then
        echo "Installing the update indicator for KDE Plasma"
            
        sudo pacman -Sy pamac-tray-icon-plasma
        
        echo "Indicator successfully installed"
    else
        echo "Additions are not necessary."
    fi
}

PS3="Select the menu option: "

select start in Execute\ the\ entire\ script Choose\ what\ you\ need; do
    case $start in
        Execute\ the\ entire\ script)
            aur_helpers
            pamac_install
        ;;
        
        Choose\ what\ you\ need)
            select aur_installer in Install\ only\ the\ AUR\ helper Install\ the\ Pamac\ graphical\ shell Exit; do
                case $aur_installer in
                    Install\ only\ the\ AUR\ helper)
                        aur_helpers
                        cas
                    ;;
                    
                    Install\ the\ Pamac\ graphical\ shell)
                        pamac_install
                        cas
                    ;;
                    
                    Exit)
                        exit
                    ;;
                esac
            done
        ;;
    esac
done