#!/usr/bin/env bash

clear

echo "Installing basic fonts..."

sudo pacman -Sy --needed noto-fonts noto-fonts-extra noto-fonts-cjk noto-fonts-emoji ttf-liberation
sleep 2

additional_font_install(){
    echo "Select additional fonts:
    1 - Ubuntu Font Family
    2 - Roboto Font Family
    3 - Iosevka Icons Nerd Font
    4 - Awesome Icons Font
    5 - Icomoon Feather Icons Font
    6 - Material Icons Font"
    
    read -r -p "$(tput bold)Select the items (1-6): $(tput sgr0)" FONT_CHOICE
    

    if [[ $FONT_CHOICE =~ [1-6] ]]; then
        if [[ $FONT_CHOICE =~ [1] ]]; then
            font_package1=ttf-ubuntu-font-family
        fi
        
        if [[ $FONT_CHOICE =~ [2] ]]; then
            font_package2=ttf-roboto
        fi
        
        if [[ $FONT_CHOICE =~ [3] ]]; then
            font_package3=ttf-iosevka-nerd
        fi
        
        if [[ $FONT_CHOICE =~ [4] ]]; then
            font_package4=ttf-font-awesome
        fi
        
        if [[ $FONT_CHOICE =~ [5] ]]; then
            font_package5=ttf-icomoon-feather
        fi
        
        if [[ $FONT_CHOICE =~ [6] ]]; then
            font_package6=ttf-material-design-icons
        fi
        FONT_INSTALL_CHOICE="${font_package1} ${font_package2} ${font_package3} ${font_package4} ${font_package5} ${font_package6}"

        aur_helper_check
        $aur_install -S $FONT_INSTALL_CHOICE
        
        if [[ $FONT_CHOICE = "" ]]; then
            :
        else
            echo "The packages are not selected!"
        fi
    else
        typing_protection
        additional_font_install
    fi
}
additional_font_install
echo "Fonts successfully installed! The anti-aliasing is set up..."

sudo sed '12s/^#//' -i /etc/profile.d/freetype2.sh

sudo cp additions/local.conf /etc/fonts/

echo -e "Xft.dpi: 96
Xft.antialias: true
Xft.hinting: true
Xft.rgba: rgb
Xft.autohint: false
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault" >> ~/.Xresources