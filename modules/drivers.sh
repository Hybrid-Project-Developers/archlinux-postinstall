#!/usr/bin/env bash

nvidia_driver_check

aur_helper_check

clear

echo "Driver installing..."

nvidia_hybrid_driver_install(){
    if [[ $(lspci -k | grep -EA2 'VGA|3D' | sed -rn 's/.*(Intel).*/\1/p') || $(lspci -k | grep -EA2 'VGA|3D' | sed -rn 's/.*(AMD).*/\1/p') ]]; then
        if [[ $nv_arch == Tesla || $nv_arch == Fermi ]]; then
            echo "Installing Bumblebee"

            sudo pacman -Sy --noconfirm bumblebee

            echo "Bumblebee succefully installed! Adding $USER to Bumblebee group..."

            sudo gpasswd -a $USER bumblebee

            echo "$USER succeful added! Enabling bumblebee service..."

            systemctl enable --now bumblebeed

            echo 'To use Bumblebee when launching applications, specify the "optirun" argument before launching'
        elif [[ $nv_arch == Kepler || $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            echo "Installing Nvidia-PRIME"

            sudo pacman -Sy nvidia-prime

            echo 'To use nvidia-prime, specify the "prime-run" argument before starting'
        fi
    else
        echo "Hybrid graphics not detected"
    fi
}

video_drivers_install(){
    echo "Video-driver install..."
    nvidia_driver_install(){
        aur_helper_check
        nvidia_driver_check

        if [[ $nv_arch == Tesla && $aur_install != "" ]]; then
            $aur_install -S nvidia-340xx-dkms nvidia-340xx-utils nvidia-340xx-settings
        elif [[ $nv_arch == Fermi ]]; then
            $aur_install -S nvidia-390xx lib32-opencl-nvidia-390xx lib32-nvidia-390xx-utils nvidia-390xx-settings
        elif [[ $nv_arch == Kepler ]]; then
            $aur_install -S nvidia-470xx-dkms nvidia-470xx-utils lib32-nvidia-470xx-utils nvidia-470xx-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia-470xx opencl-nvidia-470xx libxnvctrl-470xx
        elif [[ $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            $aur_install -S nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia opencl-nvidia libxnvctrl
        fi

        echo "Setting up the mkinitcpio modules..."

        nvidia_modules_array=("nvidia" "nvidia_modeset" "nvidia_uvm" "nvidia_drm")

        for modules in "${nvidia_modules_array[@]}"; do
            if [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep "$modules") ]]; then  
                echo "Модуль $modules уже прописан!"
            elif [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep $modules) != $modules ]]; then
                sudo sed -i 's|MODULES=(.*[^)]|& '"$modules"'|' test.txt
                echo "Модуль $modules удачно прописан!"
            fi
        done

        sudo mkinitcpio -P

        echo "Setting up nvidia-drm..."

        grub_additional_param="nvidia-drm.modeset=1"

        if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$grub_additional_param") ]]; then  
            echo "Параметр $grub_additional_param уже прописан!"
        elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $grub_additional_param) != $grub_additional_param ]]; then
            sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$grub_additional_param"'|' /etc/default/grub
            echo "Параметр $grub_additional_param удачно прописан!"
        fi

        sudo grub-mkconfig -o /boot/grub/grub.cfg

        nvidia_hybrid_driver_install
    }

    amd_driver_install(){
        if [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) = radeon ]] && [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {3,20}/ /g" | grep "Kernel modules:" | cut -d" " -f 3) = "radeon" ]]; then
            echo "Your video adapter does not support the AMDGPU driver. Installation stops."
            exit
        elif [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {3,20}/ /g" | grep "Kernel modules:" | cut -d" " -f 3) = "radeon, amdgpu" ]] || [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {3,20}/ /g" | grep "Kernel modules:" | cut -d" " -f 3) = $(grep -Pio 'Oland|Cape Verde|Pitcairn|Tahiti|Bonaire|Hawaii|Temash|Kabini|Liverpool|Durango|Kaveri|Godavari|Mullins|Beema|Carrizo-L') ]]; then
            echo "Checking the graphics card architecture..."
            radeon_gcn_check
        fi

        # GCN 1.0
        radeon_si=("radeon.si_support=0" "amdgpu.si_support=1" "amdgpu.dc=1")

        # GCN 2.0
        radeon_cik=("radeon.cik_support=0" "amdgpu.cik_support=1" "amdgpu.dc=1")

        if $radeon_architecture == "gcn1"; then
            for param in "${radeon_si[@]}"; do
                if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$param") ]]; then
                    echo "The $param parameter has been added!"
                elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $param) != $param ]]; then
                    sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$param"'|' /etc/default/grub
                    echo "Paramater $param succefully added!"
                fi
            done
        elif $radeon_architecture == "gcn2"; then
            for param in "${radeon_cik[@]}"; do
                if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$param") ]]; then
                    echo "The $param parameter has been added!"
                elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $param) != $param ]]; then
                    sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$param"'|' /etc/default/grub
                    echo "Paramater $param succefully added!"
                fi
            done
        fi

        if [[ $(lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) = amdgpu || $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "amdgpu.si_support=1|amdgpu.cik_support=1") ]]; then
            echo "Select the Vulkan driver for your video card:
            1. AMDVLK
            2. RADV"
            read -p "Select Driver: " amd_vulkan_driver

            if [[ $amd_vulkan_driver = 1 ]]; then
                amd_vlk_driver=("amdvlk" "lib32-amdvlk")
            elif [[ $amd_vulkan_driver = 2 ]]; then
                amd_vlk_driver=("vulkan-radeon" "lib32-vulkan-radeon")
            fi

            sudo pacman -S --needed ${amd_vlk_driver[@]} mesa lib32-mesa xf86-video-amdgpu libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau libva-vdpau-driver vulkan-icd-loader lib32-vulkan-icd-loader
        fi
    }

    intel_driver_install(){
        echo "This script is designed for users with newer Intel video chips that support Vulkan. The script will just install them, the system will not use them"

        echo "Installing Video-drivers for Intel"
        sudo pacman -Sy vulkan-intel vulkan-icd-loader vulkan-mesa-layers libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau libva-vdpau-driver lib32-vulkan-icd-loader
    }

    if [[ $(lspci -k | grep -EA2 'VGA|3D' | sed -rn 's/.*(NVIDIA).*/\1/p') ]]; then
        nvidia_driver_install
    elif [[ $(lspci -k | grep -EA2 'VGA|3D' | sed -rn 's/.*(AMD).*/\1/p') ]]; then
        amd_driver_install
    elif [[ $(lspci -k | grep -EA2 'VGA|3D' | sed -rn 's/.*(Intel).*/\1/p') ]]; then
        intel_driver_install
    fi
}

tablet_driver_install(){
    echo "Installing OpenTabletDriver..."

    $aur_install -S opettabletdriver

    echo "Enabling OpenTabletDriver daemons..."

    systemctl --user daemon-reload

    systemctl --user enable opentabletdriver --now
}

PS3="Select the menu option: "

select start in Execute\ the\ entire\ script Choose\ what\ you\ need; do
    case $start in
        Execute\ the\ entire\ script)
        video_drivers_config
        tablet_driver_install # готово на 100%
        ;;

        Choose\ what\ you\ need)
        select drivers_install in Install\ only\ video\ drivers Install\ only\ graphic\ tablet\ driver; do
            case $drivers_install in
                Install\ only\ video\ drivers)
                video_drivers_install
                ;;

                Install\ only\ graphic\ tablet\ driver)
                tablet_driver_install
                ;;
            esac
        done
        ;;
    esac
done